[![Firebase Logo](https://firebase.google.com/downloads/brand-guidelines/PNG/logo-standard.png)](https://firebase.google.com/)
[![Gitlab CICD](https://image.slidesharecdn.com/fosdem2017-gitlabci-170209223337/95/fosdem-2017-gitlab-ci-1-638.jpg?cb=1486679776)](https://docs.gitlab.com/ee/ci/)

##  1. Firebase-CLI (Command-Line Interface)
These are the tools for managing, viewing, and deploying the Firebase projects.
```
npm install -g firebase-tools
```

## 2. Initialize Firebase login
```
cd firebase-folder
firebase login
```
After you run the command a browser window will open asking you to log in to firebase using your Google credentials. Enter the credentials there and Firebase will sign into your system.

## 3. Initialize Firebase project into your system
Now we have to initialize the project that we created on the Firebase console into the system. For doing run the command below.
```
firebase init
```
Press down key then select
* **hosting** and **functions** for nodejs
* **hosting** for react js

![Hosting and functions](https://miro.medium.com/max/700/1*7ZSDewUfROke7nfJVbatXQ.png)

Then select Use an existing project then press enter.
![Existing project](https://miro.medium.com/max/700/1*Y94Aqt7WpAyaEupnffm3WQ.png)

### Node js
Then select javascript
![Existing project](https://miro.medium.com/max/700/1*uYLTmHygeQc9LO42ESgCnw.png)

Type yes for installing the dependencies with npm.
![Install npm](https://miro.medium.com/max/700/1*PS3R48n0-i5E-qHpy-Qgmw.png)

Then type yes for **public** and no for **elint**
![public and elint](https://miro.medium.com/max/700/1*-KUB-o_fbBcloZHvZsBxIg.png)

### React js
Select like below:
* **What do you want to use as your public directory?** `build`

* **Configure as a single-page app (rewrite all urls to /index.html)?** `Yes`

* **File build/index.html already exists. Overwrite?** `No`

Then run build to build project
```js
npm run build
```

## 4. Test the firebase app on your local system
```js
// reactjs only hosting
firebase serve --only hosting,functions
```
## 5. Deploy online
If there is an error about billing account. Go to `package.json` change nodejs version to 8 and re-deploy

If using environment variables `.env`, must set in `root/.env` for real deploy and `root/functions/.env` for local deploy.

React js when build, `.env` has included in build fodler
```
firebase deploy
```
![Deploy](https://miro.medium.com/max/700/1*4pMv_8uvAdRezV4w2l42aw.png)

## 6. CI/CD with Gitlab-Firebase
### 1. Get gitlab login token
Run command
```
firebase login:ci
```
![Firebase token](https://about.gitlab.com/images/blogimages/firebase_05.png)

### 2. Create variables
Copy login token and create gitlab CI/CD variable. Go to `Settings -> CI/CD` then create variable
![Variable](https://i.ibb.co/fQJCmbP/Screenshot-from-2021-05-01-16-32-03.png)

It's strongly recommended not commit .env files to the resposity. Therefore, any environment variables must create in CI/CD variables

### 3. Init firebase
Init firebase project with `hosting`, `hosting` and `firestore`

### 4. Integrate Gitlab CI/CD
Create `.gitlab-ci.yml`
* Node js
```yml
image: node:12.13.0-alpine
before_script:
  - npm i -g firebase-tools
deploy:
  stage: deploy
  script:
    - cd functions
    - npm install --unsafe-perm
    - cd ..
    - firebase use --token $FIREBASE_TOKEN default
    - firebase deploy -m "Pipeline $CI_PIPELINE_ID, build $CI_BUILD_ID" --non-interactive --token $FIREBASE_TOKEN
  only:
    - master
```

* React js
```yml
image: node:12.13.0-alpine
before_script:
  - npm i -g firebase-tools
deploy:
  stage: deploy
  script:
    - npm install --unsafe-perm
    - npm run build
    - firebase use --token $FIREBASE_TOKEN default
    - firebase deploy -m "Pipeline $CI_PIPELINE_ID, build $CI_BUILD_ID" --non-interactive --token $FIREBASE_TOKEN
  only:
    - master
```

### 5. Deploy
Before deploy to gitlab, go to `.gitignore`, remove `.env` to upload to gitlab responsitory
```
# dotenv environment variables file
.env
```
