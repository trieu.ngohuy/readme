<p align="center"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16651165/trieu.ngohuy.png?width=64"><img src="https://i.imgur.com/UKOiKM3.png"></p>

## 1. Quick generating an application

References link: https://expressjs.com/en/starter/generator.html

Command
```
	npx express-generator --view=ejs #using ejs as view engine
```
Run
```
	npm start
```

## 2. Application structor:
```
├── app.js # application main config file
├── .env # environment setup
├── bin
│   └── www # server setting like port
├── package.json
├── config # hodin config files extend from app.js
├── env # list environemnt files (.env)
├── logs
├── public
│   ├── images
│   ├── javascripts
│   ├── typescript
│   ├── sass
│   └── stylesheets
│       └── style.css
├── routes
│   ├── index.js
│   └── users.js
├── controllers
├── models
└── views
    ├── layouts 
    ├── errors
    ├── controller_name #specific views for each controller
    └── particals # list particals views  like header, footer..

```
