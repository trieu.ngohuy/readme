<p align="center"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16651165/trieu.ngohuy.png?width=64"><img src="https://i.imgur.com/UKOiKM3.png"></p>

## 1. Setup virtual host in apache2

Create the First Virtual Host File

	sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/example.com.conf
	sudo gedit /etc/apache2/sites-available/example.com.conf

Edit file content as below

	<VirtualHost *:80>
      DocumentRoot "/media/trieungo/Data/Sources/mondopad/frontend/web"
      DirectoryIndex index.php
    
      <Directory "/media/trieungo/Data/Sources/mondopad/frontend/web">
        Options All
        AllowOverride All
        Require all granted
      </Directory>
    </VirtualHost>

Enable the New Virtual Host Files	

	sudo a2ensite example.com.conf

Disable the default site defined in 000-default.conf	

	sudo a2dissite 000-default.conf

Restart Apache to make these changes take effect:	

	sudo systemctl restart apache2

Set Up Local Hosts File (Optional)	

If you are on a Mac or Linux computer, edit your local file with administrative privileges by typing:	

	sudo gedit /etc/hosts
			
Add virtual host domain	

	127.0.0.2   example.com

Add custom source folder (skip this test if you source is in /var/www/html)

	sudo gedit /etc/apache2/apache2.conf

Add content below

	<Directory /media/trieungo/Data/Sources/>
	Options Indexes FollowSymLinks
	AllowOverride None
	Require all granted
	</Directory>

If you get issue "Forbidden: You don't have permission to access this resource.", open file `/etc/apache2/apache2.conf` to set user and group
```
	# These need to be set in /etc/apache2/envvars
	User trieungohuy # current user
	Group trieungohuy # current user
	#whoami run this command to get current username
```
Restart Apache to make these changes take effect:

	sudo systemctl restart apache2

## 2. Setup virtual host in lampp

Allow the usage of custom virtual hosts	

	sudo gedit /opt/lampp/etc/httpd.conf

Edit as below

	# Virtual hosts
    Include etc/extra/httpd-vhosts.conf

Create a custom domain in the hosts file of your system

    sudo gedit /etc/hosts
    #Add domain
	127.1.0.1   example.com

Create your first virtual host
    
    sudo gedit /opt/lampp/etc/extra/httpd-vhosts.conf
    
Define virtual host

    <VirtualHost 127.1.0.1:80>
      DocumentRoot "/media/trieungo/Data/Sources/mondopad/frontend/web"
      DirectoryIndex index.php
    
      <Directory "/media/trieungo/Data/Sources/mondopad/frontend/web">
        Options All
        AllowOverride All
        Require all granted
      </Directory>
    </VirtualHost>
    
Change server folder (skip this if your sources are in htdocs folder):

    sudo gedit /opt/lampp/etc/httpd.conf

Edit as below (skip this if your sources are in htdocs folder):

     DocumentRoot "/media/trieungo/Data/Sources/mondopad/frontend/web"
     <Directory "/media/trieungo/Data/Sources/mondopad/frontend/web">
     
Change User & Group value from daemon to current user (skip this if your sources are in htdocs folder):

    User trieungo
    Group trieungo
    
Set permission to new server folder

    sudo chmod 777 /media/trieungo/Data/Sources
    
Reset xampp server

    sudo /opt/lampp/lampp restart

## 3. Ubuntu lampp `netstat: command not found`

Run command below	
```
sudo apt install net-tools
```

## 4. Mysql access denied for user `'root'@'localhost'`

* Add `skip-grant-tables` (skip if version 8.*)
```
	sudo nano /etc/mysql/my.cnf
```
Add the following lines at the end:
```
	[mysqld]

	skip-grant-tables
```
* Restart mysql server
```
	sudo service mysql restart
```
* Login to mysql server
```
	sudo mysql -u root
```
* Run the following commands
```
	# early version
	UPDATE mysql.user set authentication_string = PASSWORD('root') where user = 'root';
	update user set plugin="mysql_native_password" where User='root';
	FLUSH PRIVILEGES;
	# 8.0* version
	ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
	FLUSH PRIVILEGES;
```
* Remove `skip-grant-tables` (skip if version 8.*)
```
	sudo nano /etc/mysql/my.cnf
```
Remove the lines added in step 2 if you want to keep your security standards.
* Restart mysql server
```
	sudo service mysql restart
```
