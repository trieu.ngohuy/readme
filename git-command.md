<p align="center"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16651165/trieu.ngohuy.png?width=64"><img src="https://i.imgur.com/UKOiKM3.png"></p>

## 1. Rebase all message into one

Show log all commit messages

```
	git log --oneline
```

Roll back to the first commit
```
	git reset --soft <#sha#> //#sha of the first commit
```
check git status	
```
	git status
```
All files must be green, otherwise you must add manually
Merger all commit messages into one
```
	git commit --amend
```
Edit message then save

Push changes to git:	
```
	git push --force origin #banch-name
```
## 2. Git clone ssh

Create ssh key. Create multiple ssh key for multiple accounts, change comment (`ed25519`) or email (`rsa`)
```
	ssh-keygen -t ed25519 -C "<comment>" #ED25519
	ssh-keygen -t rsa -b 2048 -C "email@example.com"
```
Seperate each ssh key into different folder
```
~/.ssh/trieudevs #for trieu.devs
~/.ssh/trieungohuy #for trieu.ngohuy
```
Copy ssh key
```
	xclip -sel clip < ~/.ssh/id_ed25519.pub
	xclip -sel clip < ~/.ssh/id_rsa.pub
```
Add ssh key into gitlab account. Go to `Settings / SSH Keys` then paste it.

Fix Error: `git@github.com: Permission denied (publickey)`

* Replace ssh key in `~/.ssh/[account_folder]` to `~/.shh` folder for each account
* Close terminal then reopen
* Then `push`

Fix error: `ssh: connect to host gitlab.com port 22: Operation timed out`
* Run command to edit `.ssh/config` file
```bash
vi ~/.ssh/config
#press i to switch to edit mode
#press esc -> !w to save file
#press esc -> !x to quit vim
```
* Then fill below content
```bash
Host github.com
User git
Hostname ssh.github.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_ed25519
Port 443

Host gitlab.com
Hostname altssh.gitlab.com
User git
Port 443
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_ed25519
```
* The try again