<p align="center"><img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16651165/trieu.ngohuy.png?width=64"><img src="https://i.imgur.com/UKOiKM3.png"></p>

## 1. Generate SSL CERT files
We can create a self-signed key and certificate pair with OpenSSL in a single command:
```
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
```
The entirety of the prompts will look something like this:
```
  Country Name (2 letter code) [AU]:US
  State or Province Name (full name) [Some-State]:New York
  Locality Name (eg, city) []:New York City
  Organization Name (eg, company) [Internet Widgits Pty Ltd]:Bouncy Castles, Inc.
  Organizational Unit Name (eg, section) []:Ministry of Water Slides
  Common Name (e.g. server FQDN or YOUR name) []:server_IP_address
  Email Address []:admin@your_domain.com

```

## 2. Configuring Apache to Use SSL
Edit apache virtual host file at `/etc/apache2/site-available` to enable SSL
```
  SLEngine on

  SSLCertificateFile      /path/apache-selfsigned.crt
  SSLCertificateKeyFile   /path/apache-selfsigned.key

  <FilesMatch "\.(cgi|shtml|phtml|php)$">
                SSLOptions +StdEnvVars
  </FilesMatch>
  <Directory /usr/lib/cgi-bin>
                SSLOptions +StdEnvVars
  </Directory>
```
## 3. Add custom SSL port
Open apache2 port file at `/etc/apache2/ports.conf` to enable SSL
```
  <IfModule ssl_module>
	  Listen 3000
  </IfModule>
```